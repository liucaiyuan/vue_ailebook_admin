import defaultSettings from '@/settings'

const title = defaultSettings.title || 'AiL阅读'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
